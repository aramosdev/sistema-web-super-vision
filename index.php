<?php
    require_once 'inc/configuration.php';
    require_once './inc/path.php';
    require_once './Controller/Controller.php';
    
    $url = (isset($_GET['url'])) ? $_GET['url']."/" : "Login/index";
    $separator = explode('/',$url);
    $controller = $separator[0];
    $action=($separator[1]==null ? 'index' : $separator[1]);  

    require_once 'www/Controllers/'.$controller.'Controller.php';
    $app = new $controller;
    $app->$action();