create database super_vision;
use super_vision;

/*----------------------Tabela Login--------------------------*/

create table login_user(
	id_user int primary key auto_increment,
	permicion bool not null,
	name_user varchar(120) not null,
	login varchar(30) unique,
	senha char(32) not null,
	email_user varchar(120) not null
);
drop table login_user;

call insertUser(true, "Alberto", "beto", md5("admin"),"albertog.ramos@hotmail.com");
call insertUser(true, "admin", "admin", md5("admin"),"albertog.ramos@hotmail.com");
call selectUser("admin", "Name");


call deleteUser(0);

select * from login_user where name_user = "Alberto";
select * from login_user;

delimiter $$
	create procedure insertUser(per bool,
								name_user varchar(120),
								login varchar(60),
								pass_user char(32),
								email varchar(120)
								)
	begin
		if( (name_user != "")&&(login !="")&&(email != ""))then
			insert into login_user 
			value(null, per, name_user, login, md5(pass_user),email);
		else
			select "Por favor prencheer campos obrigatorios" as msg;
		end if;
	end $$
delimiter ;
drop procedure insertUser;

delimiter $$
	create procedure deleteUser(id int)
	begin
		delete from login_user where id_user = id;
	end $$
delimiter ;

delimiter $$
	create procedure selectUser(search varchar(120), type_search varchar(60))
	begin
		case 
			when type_search = 'Login' then
				select * from login_user where login = search;   
			when type_search ='Name' then
				select * from login_user where name_user = search;
			when type_search ='Email' then
				select * from login_user where email = search;
			else
				select * from login_user;
		end case;
	end $$
delimiter ;
drop procedure selectUser;






